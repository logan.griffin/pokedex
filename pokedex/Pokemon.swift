//
//  Pokemon.swift
//  pokedex
//
//  Created by Logan Griffin on 19/09/16.
//  Copyright © 2016 Deepsouth Technology. All rights reserved.
//

import Foundation

class Pokemon {
    
    private var _name : String
    private var _pokedexId: Int
    
    
    var name : String {
        return _name
    }
    
    var pokedexId : Int {
        return _pokedexId
    }
    
    init(name: String, pokedexId: Int) {
        self._name = name
        self._pokedexId = pokedexId
    }
    
    
    
}